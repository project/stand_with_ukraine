
CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

The purpose of the module is to display a banner on your site to show your support for Ukraine.

Customize your banner using the #stand_with_ukraine_overlay CSS ID-attribute.

 * For a full description of the module visit:
   https://www.drupal.org/project/stand_with_ukraine

 * To submit bug reports and feature suggestions, or to track changes visit:
   https://www.drupal.org/project/issues/stand_with_ukraine


REQUIREMENTS
------------

This module requires no modules outside of Drupal core.


INSTALLATION
------------

 * Install the Stand with Ukraine module as you would normally install a
   contributed Drupal module. Visit https://www.drupal.org/node/1897420 for
   further information.

 * It is recommended that you use Composer and then clear the caches.


CONFIGURATION
-------------

  * This module doesn't have any configuration.


MAINTAINERS
-----------

 * Maksym Yemets - https://www.drupal.org/u/cmd87

Supporting organization:

 * 1xINTERNET - https://www.1xinternet.de

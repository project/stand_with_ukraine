(function ($, Drupal, drupalSettings) {
  'use strict';

  document.addEventListener('DOMContentLoaded', function() {
    let body = document.getElementsByTagName('body')[0];
    let div  = document.createElement('div');
    div.id = 'stand_with_ukraine_overlay';
    div.innerHTML = '<a title="' + drupalSettings.swu.text + '" target="_blank" href="' + drupalSettings.swu.url + '">' + drupalSettings.swu.hashtag + '</a>';
    document.body.insertBefore(div, body.firstChild);
  });

})(jQuery, Drupal, drupalSettings);

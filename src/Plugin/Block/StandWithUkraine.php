<?php

namespace Drupal\stand_with_ukraine\Plugin\Block;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Provides a block with a Stand with Ukrain Banner.
 *
 * @Block(
 *   id = "stand_with_ukraine_block",
 *   admin_label = @Translation("Stand With Ukraine block"),
 * )
 */
class StandWithUkraine extends BlockBase {
  /**
   * {@inheritdoc}
   */
  public function build() {
    return [
      '#markup' => $this->t('<div id="stand_with_ukraine_overlay"><a title="Stand With Ukraine" target="_blank" href="https://war.ukraine.ua/">#StandWithUkraine</a></div>'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function blockAccess(AccountInterface $account) {
    return AccessResult::allowedIfHasPermission($account, 'access content');
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $config = $this->getConfiguration();

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['stand_with_ukraine_settings'] = $form_state->getValue('stand_with_ukraine_settings');
  }
}
